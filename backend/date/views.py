from django.shortcuts import render
from rest_framework import viewsets
from .serializers import EventSerializer, PersonSerializer
from .models import Event, Person

class EventView(viewsets.ModelViewSet):
    serializer_class = EventSerializer
    queryset = Event.objects.all()

class PersonView(viewsets.ModelViewSet):
    serializer_class = PersonSerializer
    queryset = Person.objects.all()
