from django.contrib import admin
from .models import Event, Person
# Register your models here.

class EventAdmin(admin.ModelAdmin):  # add this
    list_display = ('Title', 'Location', 'Performer', 'StartDateTime',
    'ActivityType', 'Attendees', 'Candidates') # add this

class PersonAdmin(admin.ModelAdmin):  # add this
    list_display = ('Name', 'Email', 'Password', 'Age', 'Gender', 'InterestedAgeMin', 'InterestedAgeMax',
    'InterestedGender', 'InterestedActivityType', 'Credibility', 'Attended', 'Booked', 'Recommendations') # add this

# Register your models here.
admin.site.register(Event, EventAdmin) # add this
admin.site.register(Person, PersonAdmin) # add this
