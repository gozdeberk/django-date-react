#!/usr/bin/env python
# coding: utf-8

class RDFparser:

    def __init__(self, filename):
        self.filename = filename
        import rdflib
        self.g = rdflib.Graph()
        with open(filename, "r") as f:
            result = self.g.parse(f, format="application/rdf+xml")

    def getActivityTree(self, activity):

        qres = self.g.query(
            """
            PREFIX syntax: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX datevent: <http://www.semanticweb.org/gozdeberk/ontologies/datevent#>
            PREFIX schema: <http://www.w3.org/2000/01/rdf-schema#>

            SELECT ?o ?u
            WHERE {
                datevent:"""+activity+""" syntax:type ?o .
                ?o schema:subClassOf ?u .
            }
            """)

        for o,u in qres:
            return o, u

    def getCategoryFromActivityTree(self, activity):

        qres = self.g.query(
            """
            PREFIX syntax: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX datevent: <http://www.semanticweb.org/gozdeberk/ontologies/datevent#>
            PREFIX schema: <http://www.w3.org/2000/01/rdf-schema#>

            SELECT ?o ?u
            WHERE {
                datevent:"""+activity+""" syntax:type ?o .
                ?o schema:subClassOf ?u .
            }
            """)

        for o,u in qres:
            return u

    def getTypeFromActivityTree(self, activity):

        qres = self.g.query(
            """
            PREFIX syntax: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX datevent: <http://www.semanticweb.org/gozdeberk/ontologies/datevent#>
            PREFIX schema: <http://www.w3.org/2000/01/rdf-schema#>

            SELECT ?o ?u
            WHERE {
                datevent:"""+activity+""" syntax:type ?o .
                ?o schema:subClassOf ?u .
            }
            """)

        for o,u in qres:
            return o

    def myEventDictionaries(self, myEvents):
        myCategoryDict = {}
        myTypeDict = {}
        myGenreDict = {}
        for event in myEvents:
            _type, category = self.getActivityTree(event)

            if category not in myCategoryDict:
                myCategoryDict[category] = 1.0/len(myEvents)
            else:
                myCategoryDict[category] = myCategoryDict[category] + 1.0/len(myEvents)

            if _type not in myTypeDict:
                myTypeDict[_type] = 1.0/len(myEvents)
            else:
                myTypeDict[_type] = myTypeDict[_type] + 1.0/len(myEvents)

            if event not in myGenreDict:
                myGenreDict[event] = 1.0/len(myEvents)
            else:
                myGenreDict[event] = myGenreDict[event] + 1.0/len(myEvents)

            #print(event, _type, category)

        return myCategoryDict, myTypeDict, myGenreDict

    def calculateCategoryScores(self, myCategoryDict, eventsOfOthers):
        scores = {}

        for user, events in eventsOfOthers.items():
            score = 0.0
            for event in events:
                category = self.getCategoryFromActivityTree(event)
                #print(category)
                if category in myCategoryDict:
                    score = score + myCategoryDict[category]/len(events)
            scores[user] = score

        return scores

    def calculateTypeScores(self, myTypeDict, eventsOfOthers):
        scores = {}

        for user, events in eventsOfOthers.items():
            score = 0.0
            for event in events:
                _type = self.getTypeFromActivityTree(event)
                #print(_type)
                if _type in myTypeDict:
                    score = score + myTypeDict[_type]/len(events)
            scores[user] = score

        return scores

    def calculateGenreScores(self, myGenreDict, eventsOfOthers):
        scores = {}

        for user, events in eventsOfOthers.items():
            score = 0.0
            for event in events:
                #print(event)
                if event in myGenreDict:
                    score = score + myGenreDict[event]/len(events)
            scores[user] = score

        return scores

    def getRecommendations(self, name, user_event_dict):

        #My event history
        myEvents = user_event_dict[name]

        #My category, type, genre history with frequencies
        myCategoryDict, myTypeDict, myGenreDict = self.myEventDictionaries(myEvents)

        #User list to find a recommendation
        users = list(user_event_dict.keys())
        users.remove(name)

        if(len(users) == 0):
            return None
        #For each user, find their event history
        eventsOfOthers = {}
        for user in users:
            eventsOfOthers[user] = user_event_dict[user]

        import collections

        #Calculate category similarity score for each user
        categoryScores = self.calculateCategoryScores(myCategoryDict, eventsOfOthers)

        categoryScores = {k: v for k, v in sorted(categoryScores.items(), key=lambda item: item[1], reverse=True)}

        print(categoryScores)

        #Check if there is only 1 or 2 people left
        if len(categoryScores.keys()) == 1 or len(categoryScores.keys()) == 2:
            return list(categoryScores.keys())[0]

        #Remove half of the list
        users = list(categoryScores.keys())
        del(users[-int((len(users)/2)):])
        print(users)

        #Also remove them from eventsOfOthers dictionary
        for k in list(eventsOfOthers):
            if k not in users:
                del eventsOfOthers[k]

        #Calculate type similarity score for each user
        typeScores = self.calculateTypeScores(myTypeDict, eventsOfOthers)
        typeScores = {k: v for k, v in sorted(typeScores.items(), key=lambda item: item[1], reverse=True)}

        print(typeScores)

        #Check if there is only 1 or 2 people left
        if len(typeScores.keys()) == 1 or len(typeScores.keys()) == 2:
            return list(typeScores.keys())[0]

        #Remove half of the list
        users = list(typeScores.keys())
        del(users[-int((len(users)/2)):])
        print(users)

        #Also remove them from eventsOfOthers dictionary
        for k in list(eventsOfOthers):
            if k not in users:
                del eventsOfOthers[k]

        #Calculate genre similarity score for each user
        genreScores = self.calculateGenreScores(myGenreDict, eventsOfOthers)
        genreScores = {k: v for k, v in sorted(genreScores.items(), key=lambda item: item[1], reverse=True)}

        print(genreScores)

        return list(genreScores.keys())[0]
