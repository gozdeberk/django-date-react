# date/serializers.py

from rest_framework import serializers
from .models import Event, Person

class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ('Title', 'Location', 'Performer', 'StartDateTime',
        'ActivityType', 'Attendees', 'Candidates')

class PersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = ('Name', 'Email', 'Password', 'Age', 'Gender', 'InterestedAgeMin', 'InterestedAgeMax',
        'InterestedGender', 'InterestedActivityType', 'Credibility', 'Attended', 'Booked', 'Recommendations')
