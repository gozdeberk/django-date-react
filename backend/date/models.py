from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from date.RDFparser import RDFparser

# Create your models here.
class Event(models.Model):
    Title = models.CharField(primary_key=True, max_length=120)
    Location = models.CharField(max_length=120)
    Performer = models.TextField(max_length=200)
    StartDateTime = models.DateTimeField()
    ActivityType = models.CharField(max_length=120, default='Other')
    Attendee = models.ManyToManyField('Person', related_name='attendee_list', blank=True)
    Candidate = models.ManyToManyField('Person', related_name='candidate_list', blank=True)

    def _str_(self):
        return self.Title

    def Attendees(self):
        return [a.Name for a in self.Attendee.all()]

    def Candidates(self):
        return [a.Name for a in self.Candidate.all()]

class Person(models.Model):
    Name = models.CharField(primary_key=True, max_length=120)
    Email = models.EmailField(help_text='Please enter a valid email address.')
    Password = models.CharField(max_length=8)
    Age = models.PositiveIntegerField()
    Male = 'Male'
    Female = 'Female'
    NotSpecified = 'NotSpecified'
    GenderType = [
        (Male, "Male"),
        (Female, "Female"),
        (NotSpecified, "NotSpecified"),
        ]
    Gender = models.CharField(max_length=18, choices=GenderType, default=NotSpecified)
    InterestedAgeMin = models.PositiveIntegerField()
    InterestedAgeMax = models.PositiveIntegerField()
    InterestedGender = models.CharField(max_length=18, choices=GenderType, default=NotSpecified)
    Credibility = models.PositiveIntegerField(validators=[MaxValueValidator(5), MinValueValidator(1)])
    InterestedActivityType = models.CharField(max_length=120, default='Other')

    def _str_(self):
        return self.Name

    def Attended(self):
        return [e.Title for e in self.attendee_list.all()]

    def Booked(self):
        return [e.Title for e in self.candidate_list.all()]

    def Recommendations(self):
        recommendations = {}
        parser = RDFparser('/Users/gozdeberk/Documents/dating/backend/date/datevent.rdf')
        for booked_event in self.candidate_list.all():
            user_event_dict = {}
            user_event_dict[self.Name] = [e.ActivityType for e in self.attendee_list.all()]
            for date in booked_event.Candidate.all():
                user_event_dict[date.Name] = [e.ActivityType for e in date.attendee_list.all()]
                recommendations[booked_event.Title] = parser.getRecommendations(self.Name, user_event_dict)
        return recommendations
